import axios from 'axios';
import * as actionTypes from './actionTypes';
import {reset} from 'redux-form';

export const taskRemoved = (data) => {
      return {
          type: actionTypes.TASK_REMOVED,
          data: data
      }
}

export const activeTasks = () => {
      return {
          type: actionTypes.ACTIVE_TASKS,
      }
}

export const completedTasks = () => {
      return {
          type: actionTypes.COMPLETED_TASKS,
      }
}

export const allTasks = () => {
      return {
          type: actionTypes.ALL_TASKS,
      }
}

export const editTask = (data) => {
      return {
            type: actionTypes.EDIT_TASK,
            data: data
        }
}

export const deleteAllCompletedTasks = (data) => {
      return dispatch => {
            data.forEach(element => {
                  axios.delete(`http://localhost:8080/todos/${element.id}/`)
                  .then( res => {
                        if(res.status === 200){
                              dispatch({type: "TASK_REMOVED", data: element.id})
                        }   
                  })
            });
         
      }   
}

export const setAllCompleted = (data) => {
      return dispatch => {
            data.forEach(element => {
                  axios.post(`http://localhost:8080/todos/${element.id}/complete/`)
                  .then( res => {
                        if(res.status === 200) {
                              dispatch({type: "TASK_SUCCESSFULY_COMPLETED", data: res.data})
                        }                 
                  })
            });
    
      }
}

export const updateTask = (data) => {
      return dispatch => {
            axios.post(`http://localhost:8080/todos/${data.id}/`,{
                  text: data.text
            })
            .then( res => {
                  if(res.status === 200) {
                        dispatch({type: "TASK_SUCCESSFULY_UPDATED", data: res.data})
                        dispatch(reset('task'));
                  }                 
            })
      } 
}

export const completeTask = (id) => {
      return dispatch => {
            axios.post(`http://localhost:8080/todos/${id}/complete/`)
            .then( res => {
                  if(res.status === 200) {
                        dispatch({type: "TASK_COMPLETE", data: res.data})
                  }                 
            })
      } 
}

export const incompleteTask = (id) => {
      return dispatch => {
            axios.post(`http://localhost:8080/todos/${id}/incomplete/`)
            .then( res => {
                  if(res.status === 200) {
                        dispatch({type: "TASK_INCOMPLETE", data: res.data})
                  }                 
            })
      } 
}

export const removeTask = (id) => {
      return dispatch => {
            axios.delete(`http://localhost:8080/todos/${id}/`)
            .then( res => {
                  if(res.status === 200){
                        dispatch({type: "TASK_REMOVED", data: id})
                  }   
            })
      } 
}

export const taskAdded = (data) => {
      return {
          type: actionTypes.TASK_ADDED,
          data: data
      }
}

export const fetchTasks = (dispatch) => {      
      return dispatch => {
            dispatch({type: "START_FETCHING"})
            axios.get('http://localhost:8080/todos/')
            .then( res => {
                  dispatch({type: "TASK_FETCHED", data: res.data})
            })
      } 
}

export const addTask = (value) => {
     return dispatch => {
            axios.post('http://localhost:8080/todos/', {
                  text: value,
        }).then(res => {
              dispatch(taskAdded(res.data));
              dispatch(reset('task'));
        })   
     } 
}