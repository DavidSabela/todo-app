import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

// 1 = completed , 2 = all, 3 = active 

const initialState = {
    tasks: null,
    fetching: false,
    active: null,
    completed: null,
    actual: null,
    editedTask: null,
    editing: false,
    filter: 2
}

export const taskComplete = (state, action) => {
      var tasks = state.tasks;
      var index = tasks.findIndex(x => x.id === action.data.id);
      var newTasks = tasks.map((item, i) => {
            if (i === index) {
              item = { ...item, completed: action.data.completed };
            }
            return item;
      });

      var Obj = {
            completed: action.data.completed ? 
                  state.completed.concat(action.data):
                  state.completed.filter(task => task.id !== action.data.id),
            tasks: newTasks, 
            active: action.data.completed ? 
                  state.active.filter(task => task.id !== action.data.id): 
                  state.active.concat(action.data),
            }
      switch (state.filter){
            case 1: 
                  break;
            case 2: Obj.actual = newTasks;
                  break;
            case 3: Obj.actual = state.active.filter(task => task.id !== action.data.id);    
                  break;
            default:
                  break;
      }
      
      return updateObject(state, { ...Obj })   
}

export const editTask = (state, action) => {
      return updateObject(state, {
            editedTask: action.data,
            editing: true
            }
      )
}

const findAndUpdate = (arr, data, type) => {
      var index = arr.findIndex(x => x.id === data.id);
      var newTasks = arr
      if (index !== -1){
            newTasks = arr.map((item, i) => {
                  if (i === index) {
                        if (type === "edit"){
                              item = { ...item, text: data.text };
                        } else {
                              item = { ...item, completed: data.completed };
                        }
                    
                  }
                  return item;
            });
      }
      return newTasks; 
}

export const successUpdatedTask = (state, action) => {
    
      return updateObject(state, {
            editedTask: null,
            editing: false,
            tasks: findAndUpdate(state.tasks, action.data, "edit"), 
            completed:findAndUpdate(state.completed, action.data, "edit"), 
            active: findAndUpdate(state.active, action.data, "edit"), 
            actual: findAndUpdate(state.actual, action.data, "edit")
            }
      )
}

export const taskAdded = (state, action) => {
      return updateObject(state, {
            tasks: state.tasks.concat(action.data),
            active: state.active.concat(action.data),
            actual: state.actual.concat(action.data)
            }
      )
}

export const taskRemoved = (state, action) => {

      return updateObject(state, {
            tasks: state.tasks.filter(task => task.id !== action.data),
            active: state.active.filter(task => task.id !== action.data),
            completed: state.completed.filter(task => task.id !== action.data),
            actual: state.actual.filter(task => task.id !== action.data)
            }
      )
}

export const startFetching = (state, action) => {
      return updateObject(state, {
            fetching: true
      })
}

export const tasksFetched = (state, action) => {
      return updateObject(state, {
            tasks: action.data,
            active: action.data.filter(task => task.completed !== true),
            completed: action.data.filter(task => task.completed !== false),
            actual: action.data,
            fetching: false
      })
}

export const filterAllTasks = (state, action) => {

      return updateObject(state, {
            filter: 2,
            actual: state.tasks
            }
      )
}

export const filterActiveTasks = (state, action) => {

      return updateObject(state, {
            filter: 3,
            actual: state.active
            }
      )
}

export const filterCompletedTasks = (state, action) => {

      return updateObject(state, {
            filter: 1,
            actual: state.completed
            }
      )
}

const taskReducer = (state=initialState, action) => {
      switch (action.type) {
          case actionTypes.TASK_FETCHED: return tasksFetched(state, action);
          case actionTypes.START_FETCHING: return startFetching(state, action);
          case actionTypes.TASK_ADDED: return taskAdded(state, action);
          case actionTypes.TASK_REMOVED: return taskRemoved(state, action);
          case actionTypes.TASK_COMPLETE: return taskComplete(state, action);
          case actionTypes.TASK_INCOMPLETE: return taskComplete(state, action);
          case actionTypes.EDIT_TASK: return editTask(state, action);
          case actionTypes.ALL_TASKS: return filterAllTasks(state, action);
          case actionTypes.ACTIVE_TASKS: return filterActiveTasks(state, action);
          case actionTypes.COMPLETED_TASKS: return filterCompletedTasks(state, action);
          case actionTypes.TASK_SUCCESSFULY_UPDATED: return successUpdatedTask(state, action);
          case actionTypes.TASK_SUCCESSFULY_COMPLETED: return taskComplete(state, action);
          
       
          default:
              return state;
      }
}

export default taskReducer;