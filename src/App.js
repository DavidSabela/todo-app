import React from 'react';
import './App.css';
import Form from './components/Form';

import { connect } from 'react-redux'
import * as actions from './store/actions/task';
import Task from './components/Task';

class App extends React.Component {

  
  componentWillMount(){
    this.props.fetch()
  }

  delete = () => {
    this.props.deleteAllCompleted(this.props.completed)
  }

  complete = () => {
    this.props.allCompleted(this.props.actual)
  }

  render(){
    
    const { fetching,  actual, completed} = this.props

    return (	
      
      <div className="App">
        <h1>{ completed != null ? "hotové: " + completed.length : ""}</h1>
        <Form />
        {fetching && <p>fetching ..</p>}

        <section className="todo">
          <ul className="todo-list">
            {(() =>
            {
                return actual != null ? actual.map((item, i) => (
                  <Task data={item} index={i} key={i}/>
                )): ""
          }
          )()}
          
        </ul>
        </section >
        <button onClick={this.props.getActive}>Active</button>
        <button onClick={this.props.getCompleted}>Completed</button>
        <button onClick={this.props.getAll}>All</button>
        <button onClick={this.delete}>Delete all completed</button>
        <button onClick={this.complete}>Set all completed</button>
      </div>
    );
  }

}

const mapStateToProps = (state) => {
  return {
        completed:  state.taskReducer.completed,
        fetching: state.taskReducer.fetching,
        actual: state.taskReducer.actual
  }
}

const mapDispatchToProps = dispatch => {
  return {
        fetch: () => dispatch(actions.fetchTasks()),
        add: () => dispatch(actions.addTask()),
        getActive: () => dispatch(actions.activeTasks()),
        getAll: () => dispatch(actions.allTasks()),
        getCompleted: () => dispatch(actions.completedTasks()),
        deleteAllCompleted: (data) => dispatch(actions.deleteAllCompletedTasks(data)),
        allCompleted: (data) => dispatch(actions.setAllCompleted(data))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(App);
