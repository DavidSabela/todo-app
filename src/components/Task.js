import { connect } from 'react-redux'
import * as actions from '../store/actions/task';
import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes, faEdit } from '@fortawesome/free-solid-svg-icons'

class Task extends React.Component {

      delete = () => {
            this.props.delete(this.props.data.id)
      }

      edit = () => {
            this.props.editing(this.props.data)
      }

      checkInput = (e) => {
            if (e.target.checked) {
                  this.props.complete(this.props.data.id)
            } else {
                  this.props.incomplete(this.props.data.id)
            }     
      }

      render() {
            const styleDelete = {position: 'absolute', right: '10px'}
            const styleEdit = {position: 'absolute', right: '22px'}
            const styleInput = {backgroundColor: 'yelow'}
            return (  
                  <li>
                        <input 
                              onChange={this.checkInput} 
                              type="checkbox" 
                              id={this.props.index}
                              checked={this.props.data.completed}
                        /> 
                        <label 
                              style={styleInput} 
                              className="toggle" 
                              htmlFor={this.props.index}>
                        </label>
                        {this.props.data.text}
                        <span 
                              style={styleDelete} 
                              onClick={this.delete}>
                              <FontAwesomeIcon icon={faTimes} />
                        </span>
                        <span 
                              style={styleEdit} 
                              onClick={this.edit}>
                              <FontAwesomeIcon icon={faEdit} />
                        </span>
                  </li>
            );
      }
      
}
const mapDispatchToProps = dispatch => {
      return {
            delete: (id) => dispatch(actions.removeTask(id)),
            complete: (id) => dispatch(actions.completeTask(id)),
            incomplete: (id) => dispatch(actions.incompleteTask(id)),
            editing: (data) => dispatch(actions.editTask(data))
      }
}

export default connect(null, mapDispatchToProps)(Task)
