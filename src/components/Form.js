import React from 'react'
import { reduxForm, Field } from 'redux-form'
import { connect } from 'react-redux';
import * as actions from '../store/actions/task';

class Form extends React.Component {

      postData = (value, dispatch) => {
             if(this.props.initialValues != null){
                  dispatch(actions.updateTask(value));
             } else {
                  dispatch(actions.addTask(value.text));
            }            
      }

      render() {
            const { handleSubmit } = this.props;
            return(
                  <form onSubmit={handleSubmit(this.postData)}>
                  <div>
                        <label>Pridat task</label>
                        <div>
                              <Field
                                    name="text"
                                    component="input"
                                    type="text"
                                    placeholder="novy task"
                              />
                        </div>
                  </div>
                  <div>
                        <button type="submit" >
                              Potvrdit
                        </button>
                  </div>
            </form>  
            );
      }
}


const mapStateToProps = (state) => {
      return {
            initialValues: state.taskReducer.editedTask,
      }
}

Form = reduxForm({
      form: 'task',
      destroyOnUnmount: true,
})(Form)

Form = connect(mapStateToProps, null)(Form)

export default Form


